import { Provider } from 'mobx-react';
import Devtools from 'mobx-react-devtools';
import * as React from 'react';

import Munchkin from './munchkin';
import store from './store';

import './App.css';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Devtools />
        <header className="App-header">
          <h1 className="App-title">Munchkin Counter</h1>
        </header>
        <Provider munchkin={store}>
            <Munchkin />
        </Provider>
      </div>
    );
  }
}

export default App;

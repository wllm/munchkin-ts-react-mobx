import * as React from 'react';

import GearInput from '../gear-input';
import GenderInput from '../gender-input';
import LevelInput from '../level-input';
import NameInput from '../name-input';
import Power from '../power';

import './Munchkin.css';

const Munchkin = () => (
    <main className="Munchkin-container">
        <p className="Munchkin-name-gender-help-text">
            Start by giving your munchkin a name and fill out your munchkins gender.
            Some parts of the game depend on which gender your munchkin has, and their
            gender may change during the course of the game!
        </p>
        <NameInput className="Munchkin-name-input"/>
        <GenderInput className="Munchkin-gender-input"/>
        <p className="Munchkin-level-gear-help-text">
            Keep track of your level and gear during play
        </p>
        <LevelInput className="Munchkin-level-input" />
        <GearInput className="Munchkin-gear-input"/>
        <p className="Munchkin-power-help-text">
            This is your total power. Kick some monster butt!
        </p>
        <Power className="Munchkin-power" />
    </main>
);

export default Munchkin;

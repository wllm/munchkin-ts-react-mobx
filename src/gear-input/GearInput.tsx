import classNames from 'classnames';
import { action } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import './GearInput.css';

import { IMunchkin } from '../store';

interface IInjectedProps {
    munchkin: IMunchkin,
}

@inject('munchkin')
@observer
class GearInput extends React.Component<React.HTMLAttributes<HTMLDivElement>, {}> {
    public render() {
        return (
            <div
                className={classNames(
                    this.props.className,
                )}
            >
                <label
                    className="GearInput-label"
                    htmlFor="mc-gear-input"
                >
                    Gear
                </label>
                <div className="GearInput-group">
                    <button
                        aria-label="Decrease gear"
                        className="GearInput-decrease"
                        type="button"
                        onClick={this.onLevelDecreaseClick}
                    >
                        -
                    </button>
                    <input
                        className="GearInput-input-field"
                        id="mc-gear-input"
                        onChange={this.onGearChange}
                        value={this.injected.munchkin.gear}
                        type="tel"
                    />
                    <button
                        aria-label="Increase gear"
                        className="GearInput-increase"
                        type="button"
                        onClick={this.onLevelIncreaseClick}
                    >
                        +
                    </button>
                </div>
            </div>
        );
    }

    private get injected() {
        return this.props as IInjectedProps;
    }

    @action("changed munchkin gear input field")
    private onGearChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        try {
            const value = parseInt(e.target.value, 10);
            if (value >= 0) {
                this.injected.munchkin.gear = value;
            }
        } catch (e) {
            // do nothing with input
        }
    };

    @action("decreased munchkin gear")
    private onLevelDecreaseClick = () => {
        // The rules say gear 1 is the minimum gear
        if (this.injected.munchkin.gear === 0) {
            return;
        }
        this.injected.munchkin.gear--;
    };

    @action("increased munchkin gear")
    private onLevelIncreaseClick = () => {
        this.injected.munchkin.gear++;
    };
}

export default GearInput;


import { computed, observable } from 'mobx';

export interface IMunchkin {
    name: string;
    gender: string;
    level: number;
    gear: number;
    power: number;
}

class Munchkin {
    @observable public name: string = "";
    @observable public gender: string = "";
    @observable public level: number = 1;
    @observable public gear: number = 0;

    @computed get power() : number {
        return this.level + this.gear;
    };
}

export default new Munchkin();

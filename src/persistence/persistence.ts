import { reaction } from 'mobx';
import store from '../store';

const persistenceKey = 'WKNO:ts-munchkin-counter';

export const persist = () =>
    reaction(
        () => ({
            gear: store.gear,
            gender: store.gender,
            level: store.level,
            name: store.name,
        }),
        (toStore) => {
            const serialized = JSON.stringify(toStore);
            localStorage.setItem(persistenceKey, serialized);
        },
        {
            delay: 3000, // delay so spamming up/down on level or gear doesn't trigger IO for each change
            name: 'persist munchkin in localstorage',
            onError: error => console.error(error), // tslint:disable-line
        },
    );

export const restore = () => {
    try {
        const serialized = localStorage.getItem(persistenceKey);
        if (!serialized) {
            return;
        }
        const parsed: {
            gear: number,
            gender: string,
            level: number,
            name: string,
        } = JSON.parse(serialized);
        store.gear = parsed.gear;
        store.gender = parsed.gender;
        store.level = parsed.level;
        store.name = parsed.name;
    } catch (e) {
        // fail silently, present user with clean slate
    }
};

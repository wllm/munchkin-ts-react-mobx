import classNames from 'classnames';
import { action, runInAction } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import { IMunchkin } from '../store';

import './NameInput.css';

interface IInjectedProps {
    munchkin: IMunchkin;
}

@inject('munchkin')
@observer
class NameInput extends React.Component<React.HTMLAttributes<HTMLDivElement>, {}> {
    public render() {
        return (
            <div
                className={classNames(
                    this.props.className,
                )}
            >
                <label
                    className="NameInput-label"
                    htmlFor="mc-name-input"
                >
                    Name
                </label>
                <div className="NameInput-group">
                    <input
                        className="NameInput-input-field"
                        id="mc-name-input"
                        value={this.injected.munchkin.name}
                        onChange={this.onNameChange}
                    />
                    <button
                        className="NameInput-generate-name-button"
                        onClick={this.onGenerateNameClick}
                    >
                        Generate
                    </button>
                </div>
            </div>
        );
    }

    private get injected() {
        return this.props as IInjectedProps;
    }

    @action('update name')
    private onGenerateNameClick = async () => {
          try {
              const response = await fetch('http://faker.hook.io/?property=name.firstName');
              const name = await response.json();
              runInAction(() => {
                    this.injected.munchkin.name = name;
              });
          } catch (e) {
              console.error(e); // tslint:disable-line
          }
    };

    @action('changed munchkin name')
    private onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.injected.munchkin.name = e.target.value;
    };
}

export default NameInput;

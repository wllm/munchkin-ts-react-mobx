import classNames from 'classnames';
import { action } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import './LevelInput.css';

import { IMunchkin } from '../store';

interface IInjectedProps {
    munchkin: IMunchkin,
}

@inject('munchkin')
@observer
class LevelInput extends React.Component<React.HTMLAttributes<HTMLDivElement>> {
    public render() {
        return (
            <div
                className={classNames(
                    this.props.className,
                )}
            >
                <label
                    className="LevelInput-label"
                    htmlFor="mc-level-input"
                >
                    Level
                </label>
                <div className="LevelInput-group">
                    <button
                        aria-label="Decrease level"
                        className="LevelInput-decrease"
                        type="button"
                        onClick={this.onLevelDecreaseClick}
                    >
                        -
                    </button>
                    <input
                        className="LevelInput-input-field"
                        id="mc-level-input"
                        onChange={this.onLevelChange}
                        value={this.injected.munchkin.level}
                        type="tel"
                    />
                    <button
                        aria-label="Increase level"
                        className="LevelInput-increase"
                        type="button"
                        onClick={this.onLevelIncreaseClick}
                    >
                        +
                    </button>
                </div>
            </div>
        );
    }

    private get injected() {
        return this.props as IInjectedProps;
    }

    @action("changed munchkin level input field")
    private onLevelChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        try {
            const value = parseInt(e.target.value, 10);
            if (value > 0 && value <= 10) {
                this.injected.munchkin.level = value;
            }
        } catch (e) {
            // do nothing with input
        }
    };

    @action("decreased munchkin level")
    private onLevelDecreaseClick = () => {
        // The rules say level 1 is the minimum level
        if (this.injected.munchkin.level === 1) {
            return;
        }
        this.injected.munchkin.level--;
    };

    @action("increased munchkin level")
    private onLevelIncreaseClick = () => {
        // The rules say level 10 is the max level
        if (this.injected.munchkin.level === 10) {
            return;
        }
        this.injected.munchkin.level++;
    };
}

export default LevelInput;


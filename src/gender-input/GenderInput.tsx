import classNames from 'classnames';
import { action } from 'mobx';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import { IMunchkin } from '../store';

import './GenderInput.css';

interface IInjectedProps {
    munchkin: IMunchkin;
}

@inject('munchkin')
@observer
class GenderInput extends React.Component<React.HTMLAttributes<HTMLDivElement>, {}> {
    public render() {
        return (
            <div
                className={classNames(
                    this.props.className,
                )}
            >
                <label
                    className="GenderInput-label"
                    htmlFor="mc-gender-input"
                >
                    Gender
                </label>
                <input
                    className="GenderInput-input-field"
                    id="mc-gender-input"
                    value={this.injected.munchkin.gender}
                    onChange={this.onGenderChange}
                />
            </div>
        );
    }

    private get injected() {
        return this.props as IInjectedProps;
    }

    @action("changed munchkin gender")
    private onGenderChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.injected.munchkin.gender = e.target.value;
    };
}

export default GenderInput;

import classNames from 'classnames';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import { IMunchkin } from '../store';

import './Power.css';

interface IProps {
    munchkin?: IMunchkin;
}

const Power: React.SFC<IProps & React.HTMLAttributes<HTMLParagraphElement>> = (props) => (
    <p
        className={classNames(
            props.className,
        )}
    >
        <span
            className="Power-label"
        >
            Power
        </span>
        <span
            className="Power-value"
        >
            {props.munchkin ? props.munchkin.power : 1}
        </span>
    </p>
);

export default inject('munchkin')(observer(Power));
